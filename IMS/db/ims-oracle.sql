prompt PL/SQL Developer import file
prompt Created on 2017年2月9日 by 蓝枫
set feedback off
set define off
prompt Creating SYS_CATALOG...
create table SYS_CATALOG
(
  catalog_id     VARCHAR2(50) not null,
  cascade_id     VARCHAR2(500),
  root_key       VARCHAR2(100),
  root_name      VARCHAR2(100),
  catalog_name   VARCHAR2(50),
  parent_id      VARCHAR2(50),
  sort_no        NUMBER(10),
  icon_name      VARCHAR2(50),
  is_auto_expand VARCHAR2(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_CATALOG
  is '分类科目';
comment on column SYS_CATALOG.catalog_id
  is '分类科目编号';
comment on column SYS_CATALOG.cascade_id
  is '分类科目语义ID';
comment on column SYS_CATALOG.root_key
  is '科目标识键';
comment on column SYS_CATALOG.root_name
  is '科目名称';
comment on column SYS_CATALOG.catalog_name
  is '分类名称';
comment on column SYS_CATALOG.parent_id
  is '父节点编号';
comment on column SYS_CATALOG.sort_no
  is '排序号';
comment on column SYS_CATALOG.icon_name
  is '图标名称';
comment on column SYS_CATALOG.is_auto_expand
  is '是否自动展开';
comment on column SYS_CATALOG.create_time
  is '创建时间';
comment on column SYS_CATALOG.create_user_id
  is '创建用户ID';
comment on column SYS_CATALOG.modify_time
  is '修改时间';
comment on column SYS_CATALOG.modify_user_id
  is '修改用户ID';
alter table SYS_CATALOG
  add constraint PK_CATALOG_ID primary key (CATALOG_ID);

prompt Creating SYS_DEPT...
create table SYS_DEPT
(
  dept_id        VARCHAR2(50) not null,
  cascade_id     VARCHAR2(255) not null,
  dept_name      VARCHAR2(100) not null,
  parent_id      VARCHAR2(50) not null,
  dept_code      VARCHAR2(50),
  manager        VARCHAR2(50),
  phone          VARCHAR2(50),
  fax            VARCHAR2(50),
  address        VARCHAR2(200),
  is_auto_expand VARCHAR2(10),
  icon_name      VARCHAR2(50),
  sort_no        NUMBER(10),
  remark         VARCHAR2(400),
  is_del         VARCHAR2(10) default 0,
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_DEPT
  is '组织机构';
comment on column SYS_DEPT.dept_id
  is '流水号';
comment on column SYS_DEPT.cascade_id
  is '节点语义ID';
comment on column SYS_DEPT.dept_name
  is '组织名称';
comment on column SYS_DEPT.parent_id
  is '父节点流水号';
comment on column SYS_DEPT.dept_code
  is '机构代码';
comment on column SYS_DEPT.manager
  is '主要负责人';
comment on column SYS_DEPT.phone
  is '部门电话';
comment on column SYS_DEPT.fax
  is '传真';
comment on column SYS_DEPT.address
  is '地址';
comment on column SYS_DEPT.is_auto_expand
  is '是否自动展开';
comment on column SYS_DEPT.icon_name
  is '节点图标文件名称';
comment on column SYS_DEPT.sort_no
  is '排序号';
comment on column SYS_DEPT.remark
  is '备注';
comment on column SYS_DEPT.is_del
  is '是否已删除 0有效 1删除';
comment on column SYS_DEPT.create_time
  is '创建时间';
comment on column SYS_DEPT.create_user_id
  is '创建人ID';
comment on column SYS_DEPT.modify_time
  is '修改时间';
comment on column SYS_DEPT.modify_user_id
  is '修改用户ID';
alter table SYS_DEPT
  add constraint PK_DEPT_ID primary key (DEPT_ID);

prompt Creating SYS_DICTIONARY...
create table SYS_DICTIONARY
(
  dic_id         VARCHAR2(50) not null,
  dic_index_id   VARCHAR2(255),
  dic_code       VARCHAR2(100),
  dic_value      VARCHAR2(100),
  show_color     VARCHAR2(50),
  status         VARCHAR2(10) default 1,
  edit_mode      VARCHAR2(10) default 1,
  sort_no        NUMBER(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_DICTIONARY
  is '数据字典';
comment on column SYS_DICTIONARY.dic_id
  is '字典编号';
comment on column SYS_DICTIONARY.dic_index_id
  is '所属字典流水号';
comment on column SYS_DICTIONARY.dic_code
  is '字典对照码';
comment on column SYS_DICTIONARY.dic_value
  is '字典对照值';
comment on column SYS_DICTIONARY.show_color
  is '显示颜色';
comment on column SYS_DICTIONARY.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_DICTIONARY.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_DICTIONARY.sort_no
  is '排序号';
comment on column SYS_DICTIONARY.create_time
  is '创建时间';
comment on column SYS_DICTIONARY.create_user_id
  is '创建用户编号';
comment on column SYS_DICTIONARY.modify_time
  is '修改时间';
comment on column SYS_DICTIONARY.modify_user_id
  is '修改用户ID';
alter table SYS_DICTIONARY
  add constraint PK_DIC_ID primary key (DIC_ID);

prompt Creating SYS_DICTIONARY_INDEX...
create table SYS_DICTIONARY_INDEX
(
  dic_index_id       VARCHAR2(50) not null,
  dic_key            VARCHAR2(50),
  dic_name           VARCHAR2(100),
  catalog_id         VARCHAR2(50),
  catalog_cascade_id VARCHAR2(500),
  dic_remark         VARCHAR2(500),
  create_time        DATE,
  create_user_id     VARCHAR2(50),
  modify_time        DATE,
  modify_user_id     VARCHAR2(50)
)
;
comment on table SYS_DICTIONARY_INDEX
  is '数据字单类型';
comment on column SYS_DICTIONARY_INDEX.dic_index_id
  is '流水号';
comment on column SYS_DICTIONARY_INDEX.dic_key
  is '字典标识';
comment on column SYS_DICTIONARY_INDEX.dic_name
  is '字典名称';
comment on column SYS_DICTIONARY_INDEX.catalog_id
  is '所属分类流水号';
comment on column SYS_DICTIONARY_INDEX.catalog_cascade_id
  is '所属分类流节点语义ID';
comment on column SYS_DICTIONARY_INDEX.dic_remark
  is '备注';
comment on column SYS_DICTIONARY_INDEX.create_time
  is '创建时间';
comment on column SYS_DICTIONARY_INDEX.create_user_id
  is '创建用户编号';
comment on column SYS_DICTIONARY_INDEX.modify_time
  is '修改时间';
comment on column SYS_DICTIONARY_INDEX.modify_user_id
  is '修改用户ID';
alter table SYS_DICTIONARY_INDEX
  add constraint PK_DIC_INDEX_ID primary key (DIC_INDEX_ID);

prompt Creating SYS_MENU...
create table SYS_MENU
(
  menu_id        VARCHAR2(50) not null,
  cascade_id     VARCHAR2(500),
  menu_name      VARCHAR2(100),
  parent_id      VARCHAR2(50),
  icon_name      VARCHAR2(50),
  is_auto_expand VARCHAR2(10) default 0,
  url            VARCHAR2(100),
  remark         VARCHAR2(500),
  status         VARCHAR2(10) default 1,
  edit_mode      VARCHAR2(10) default 1,
  sort_no        NUMBER(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_MENU
  is '菜单配置';
comment on column SYS_MENU.menu_id
  is '菜单编号';
comment on column SYS_MENU.cascade_id
  is '分类科目语义ID';
comment on column SYS_MENU.menu_name
  is '菜单名称';
comment on column SYS_MENU.parent_id
  is '菜单父级编号';
comment on column SYS_MENU.icon_name
  is '图标名称';
comment on column SYS_MENU.is_auto_expand
  is '是否自动展开';
comment on column SYS_MENU.url
  is 'url地址';
comment on column SYS_MENU.remark
  is '备注';
comment on column SYS_MENU.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_MENU.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_MENU.sort_no
  is '排序号';
comment on column SYS_MENU.create_time
  is '创建时间';
comment on column SYS_MENU.create_user_id
  is '创建用户编号';
comment on column SYS_MENU.modify_time
  is '修改时间';
comment on column SYS_MENU.modify_user_id
  is '修改用户ID';
alter table SYS_MENU
  add constraint PK_MENU primary key (MENU_ID);

prompt Creating SYS_PARAM...
create table SYS_PARAM
(
  param_id           VARCHAR2(50) not null,
  param_name         VARCHAR2(100),
  param_key          VARCHAR2(50),
  param_value        VARCHAR2(500),
  catalog_id         VARCHAR2(50),
  catalog_cascade_id VARCHAR2(500),
  param_remark       VARCHAR2(200),
  status             VARCHAR2(10) default 1,
  edit_mode          VARCHAR2(10) default 1,
  create_time        DATE,
  create_user_id     VARCHAR2(50),
  modify_time        DATE,
  modify_user_id     VARCHAR2(50)
)
;
comment on table SYS_PARAM
  is '键值参数';
comment on column SYS_PARAM.param_id
  is '参数编号';
comment on column SYS_PARAM.param_name
  is '参数名称';
comment on column SYS_PARAM.param_key
  is '参数键名';
comment on column SYS_PARAM.param_value
  is '参数键值';
comment on column SYS_PARAM.catalog_id
  is '目录ID';
comment on column SYS_PARAM.catalog_cascade_id
  is '分类科目语义ID';
comment on column SYS_PARAM.param_remark
  is '参数备注';
comment on column SYS_PARAM.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_PARAM.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_PARAM.create_time
  is '创建时间';
comment on column SYS_PARAM.create_user_id
  is '创建用户编号';
comment on column SYS_PARAM.modify_time
  is '修改时间';
comment on column SYS_PARAM.modify_user_id
  is '修改用户ID';
alter table SYS_PARAM
  add constraint PK_PARAM_ID primary key (PARAM_ID);

prompt Creating SYS_ROLE...
create table SYS_ROLE
(
  role_id        VARCHAR2(50) not null,
  role_name      VARCHAR2(100) not null,
  status         VARCHAR2(10) default 1,
  role_type      VARCHAR2(10),
  role_remark    VARCHAR2(400),
  edit_mode      VARCHAR2(10) default 1,
  create_user_id VARCHAR2(50),
  create_time    DATE,
  modify_user_id VARCHAR2(50),
  modify_time    DATE
)
;
comment on table SYS_ROLE
  is '角色管理';
comment on column SYS_ROLE.role_id
  is '角色编号';
comment on column SYS_ROLE.role_name
  is '角色名称';
comment on column SYS_ROLE.status
  is '当前状态 1启用 0禁用';
comment on column SYS_ROLE.role_type
  is '角色类型';
comment on column SYS_ROLE.role_remark
  is '备注';
comment on column SYS_ROLE.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_ROLE.create_user_id
  is '创建用户编号';
comment on column SYS_ROLE.create_time
  is '创建时间';
comment on column SYS_ROLE.modify_user_id
  is '修改用户编号';
comment on column SYS_ROLE.modify_time
  is '修改时间';
alter table SYS_ROLE
  add constraint PK_ROLE_ID primary key (ROLE_ID);

prompt Creating SYS_ROLE_MENU...
create table SYS_ROLE_MENU
(
  role_id        VARCHAR2(50) not null,
  menu_id        VARCHAR2(50) not null,
  grant_type     VARCHAR2(10),
  create_user_id VARCHAR2(50),
  create_time    DATE
)
;
comment on table SYS_ROLE_MENU
  is '角色与菜单关联表';
comment on column SYS_ROLE_MENU.role_id
  is '角色编号';
comment on column SYS_ROLE_MENU.menu_id
  is '菜单编号';
comment on column SYS_ROLE_MENU.grant_type
  is '权限类型 1 经办权限 2管理权限';
comment on column SYS_ROLE_MENU.create_user_id
  is '创建用户ID';
comment on column SYS_ROLE_MENU.create_time
  is '创建时间';

prompt Creating SYS_ROLE_USER...
create table SYS_ROLE_USER
(
  role_id        VARCHAR2(50) not null,
  user_id        VARCHAR2(50) not null,
  create_user_id VARCHAR2(50),
  create_time    DATE
)
;
comment on table SYS_ROLE_USER
  is '角色与用户关联表';
comment on column SYS_ROLE_USER.role_id
  is '角色编号';
comment on column SYS_ROLE_USER.user_id
  is '用户编号';
comment on column SYS_ROLE_USER.create_user_id
  is '创建用户ID';
comment on column SYS_ROLE_USER.create_time
  is '创建时间';

prompt Creating SYS_USER...
create table SYS_USER
(
  user_id        VARCHAR2(50) not null,
  account        VARCHAR2(50),
  password       VARCHAR2(50),
  username       VARCHAR2(50),
  lock_num       NUMBER(10) default 5,
  error_num      NUMBER(10) default 0,
  sex            VARCHAR2(10) default 3,
  status         VARCHAR2(10) default 1,
  user_type      VARCHAR2(10),
  dept_id        VARCHAR2(50),
  mobile         VARCHAR2(50),
  qq             VARCHAR2(50),
  wechat         VARCHAR2(50),
  email          VARCHAR2(50),
  idno           VARCHAR2(50),
  style          VARCHAR2(10) default 1,
  address        VARCHAR2(200),
  remark         VARCHAR2(400),
  is_del         VARCHAR2(10) default 0,
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_USER
  is '用户管理';
comment on column SYS_USER.user_id
  is '用户编号';
comment on column SYS_USER.account
  is '用户登录帐号';
comment on column SYS_USER.password
  is '密码';
comment on column SYS_USER.username
  is '用户姓名';
comment on column SYS_USER.lock_num
  is '锁定次数 默认5次';
comment on column SYS_USER.error_num
  is '密码错误次数  如果等于锁定次数就自动锁定用户';
comment on column SYS_USER.sex
  is '性别  1:男2:女3:未知';
comment on column SYS_USER.status
  is '用户状态 1:正常2:停用 3:锁定';
comment on column SYS_USER.user_type
  is '用户类型';
comment on column SYS_USER.dept_id
  is '所属部门流水号';
comment on column SYS_USER.mobile
  is '联系电话';
comment on column SYS_USER.qq
  is 'QQ号码';
comment on column SYS_USER.wechat
  is '微信';
comment on column SYS_USER.email
  is '邮箱';
comment on column SYS_USER.idno
  is '身份证号码';
comment on column SYS_USER.style
  is '界面风格';
comment on column SYS_USER.address
  is '联系地址';
comment on column SYS_USER.remark
  is '备注';
comment on column SYS_USER.is_del
  is '是否已删除 0有效 1删除';
comment on column SYS_USER.create_time
  is '创建时间';
comment on column SYS_USER.create_user_id
  is '创建人ID';
comment on column SYS_USER.modify_time
  is '修改时间';
comment on column SYS_USER.modify_user_id
  is '修改用户编号';
alter table SYS_USER
  add constraint PK_USER_ID primary key (USER_ID);

prompt Loading SYS_CATALOG...
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('0e6cca42523b4f95afb8d138dc533e61', '0.002', 'DIC_TYPE', '字典分类科目', '数据字典分类', '0', 1, 'book', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('4f39839093744dccaedc9d7dcdee4ab3', '0.001', 'PARAM_TYPE', '参数分类', '参数分类科目', '0', 1, 'book', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('5423b9ba9ac6472c80881827acafe9e9', '0.001.001', 'PARAM_TYPE', '参数分类', '系统参数', '4f39839093744dccaedc9d7dcdee4ab3', 1, 'folder', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('6a75051434b840a597aeb549e1e47ced', '0.002.001', 'DIC_TYPE', '字典分类科目', '系统管理', '0e6cca42523b4f95afb8d138dc533e61', 2, 'folder', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('b420860910f54c1d8901f099a9457f52', '0.002.002', 'DIC_TYPE', '字典分类科目', '全局通用', '0e6cca42523b4f95afb8d138dc533e61', 1, 'global', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
commit;
prompt 5 records loaded
prompt Loading SYS_DEPT...
insert into SYS_DEPT (dept_id, cascade_id, dept_name, parent_id, dept_code, manager, phone, fax, address, is_auto_expand, icon_name, sort_no, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('0', '0', '组织机构', '-1', null, null, null, null, null, '1', 'dept_config', 1, '顶级机构不能进行移动和删除操作，只能进行修改', '0', to_date('18-01-2017 15:20:31', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DEPT (dept_id, cascade_id, dept_name, parent_id, dept_code, manager, phone, fax, address, is_auto_expand, icon_name, sort_no, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('304184e5156d4a4abb7a7218ee84f2e2', '0.0001', '23232332', '0', '3是范德萨发', null, null, null, null, '1', null, 1, null, '1', to_date('19-01-2017 11:08:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('22-01-2017 13:52:48', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 2 records loaded
prompt Loading SYS_DICTIONARY...
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('11b823f3b2e14e76bf94347a4a5e578e', 'c48507ef391d4e3d8d9b7720efe4841b', '0', '停用', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('293adbde400f457a8d947ff5c6341b04', '992a7d6dbe7f4009b30cbae97c3b64a9', '3', '锁定', '#FFA500', '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('2ac97527c4924127b742dd953d8b53ba', '820d2a68425b4d8d9b423b81d6a0eec1', '3', '未知', null, '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('2bfc90a6917545cd87d73fb491292e2b', 'aaec0092a25b485f90c20898e9d6765d', '1', '缺省', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('3cf6af08f48e4cec913d09f67a0b3b43', '992a7d6dbe7f4009b30cbae97c3b64a9', '1', '正常', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('913ca1b4b49a434fb9591f6df0a52af8', 'c6f8b99b95c844b89dc86c143e04a294', '0', '否', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('9c63657b98c444e3bfd8a0a75128de2b', '7a7faf68a5ec4f3cb9f45d89c119b26b', '0', '只读', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('a96dfb72b7b54e1989569a2b3c5f90ac', 'c48507ef391d4e3d8d9b7720efe4841b', '1', '启用', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('ca40ef37acef49f8930fcf22356ba50e', 'c6f8b99b95c844b89dc86c143e04a294', '1', '是', null, '1', '0', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d2cf230ce49040e3bf6e61a972659c09', '992a7d6dbe7f4009b30cbae97c3b64a9', '2', '停用', 'red', '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d404e540aab945df84a26e3d30b2dd47', '820d2a68425b4d8d9b423b81d6a0eec1', '2', '女', null, '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d7f0c4a5480d4dc4b3e6e4c5b405d9cb', '820d2a68425b4d8d9b423b81d6a0eec1', '1', '男', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('f1c0ae8844504f96836b904ce81ac1bc', '7a7faf68a5ec4f3cb9f45d89c119b26b', '1', '可编辑', null, '1', '0', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('e0e59a52f42c4263aac3e9dbbdb496df', '0bf2a3cd7ed44516a261347d47995411', '1', '经典风格', null, '1', '1', 1, to_date('06-02-2017 14:27:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:27:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('82afb0bda8944af3a0e5f82608294670', '0bf2a3cd7ed44516a261347d47995411', '2', '顶部布局', null, '1', '1', 2, to_date('06-02-2017 14:29:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:55:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 15 records loaded
prompt Loading SYS_DICTIONARY_INDEX...
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('7a7faf68a5ec4f3cb9f45d89c119b26b', 'edit_mode', '编辑模式', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('820d2a68425b4d8d9b423b81d6a0eec1', 'sex', '性别', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('992a7d6dbe7f4009b30cbae97c3b64a9', 'user_status', '用户状态', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('aaec0092a25b485f90c20898e9d6765d', 'role_type', '角色类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('c48507ef391d4e3d8d9b7720efe4841b', 'status', '当前状态', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('c6f8b99b95c844b89dc86c143e04a294', 'is_auto_expand', '是否自动展开', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('0bf2a3cd7ed44516a261347d47995411', 'layout_style', '界面布局', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('06-02-2017 14:27:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:27:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 7 records loaded
prompt Loading SYS_MENU...
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('1ae9eeb251a243abb4c0c4f3865d6262', '0.001.001', '菜单配置', 'c66886c9ee47415aa81a6589acdb480a', 'menu_config', '1', 'system/menu/init.jhtml', null, '1', '1', 4, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('8eefe6e3298c4203b21e85e354b284ab', '0.001.004', '分类科目', 'c66886c9ee47415aa81a6589acdb480a', 'catalog', '1', 'system/catalog/init.jhtml', null, '1', '1', 7, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('94ca69dd59b84054ad056b130d959425', '0.001.003', '键值参数', 'c66886c9ee47415aa81a6589acdb480a', 'param_config', '1', 'system/param/init.jhtml', null, '1', '1', 6, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('a5b39c90931b4249a23e30f24303dbfa', '0.001.002', '数据字典', 'c66886c9ee47415aa81a6589acdb480a', 'dictionary', '1', 'system/dictionary/init.jhtml', null, '1', '0', 5, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('c66886c9ee47415aa81a6589acdb480a', '0.001', '系统管理', '0', 'system_manage', '1', null, null, '1', '1', 10, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('23-01-2017 17:19:34', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d525b1f3274244c9af3a06c4e72621d8', '0.001.009', '角色管理', 'c66886c9ee47415aa81a6589acdb480a', 'group_link', '1', 'system/role/init.jhtml', null, '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('dfc4df3c7f4341f885caf7aa305f8995', '0.001.007', ' 组织机构', 'c66886c9ee47415aa81a6589acdb480a', 'dept_config', '1', 'system/dept/init.jhtml', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('f278c724ce8e4649bc2b74333ac0d28c', '0.001.008', '用户管理', 'c66886c9ee47415aa81a6589acdb480a', 'user_config', '1', 'system/user/init.jhtml', null, '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
commit;
prompt 8 records loaded
prompt Loading SYS_PARAM...
prompt Table is empty
prompt Loading SYS_ROLE...
prompt Table is empty
prompt Loading SYS_ROLE_MENU...
prompt Table is empty
prompt Loading SYS_ROLE_USER...
prompt Table is empty
prompt Loading SYS_USER...
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('31ee508880ef4b27bea9d95d3156fc8a', 't1', 'FyzQw6C0Ct4=', '112', 5, 0, '3', '1', '1', '0', null, null, null, null, null, null, null, null, '1', to_date('18-01-2017 12:31:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('18-01-2017 12:35:10', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('9073ca4cf33e49cbba7c1d44db9a2bc4', 't2', 'FyzQw6C0Ct4=', '1', 5, 0, '3', '1', '1', '0', null, null, null, null, null, null, null, null, '1', to_date('18-01-2017 12:32:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('18-01-2017 12:32:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('cb33c25f5c664058a111a9b876152317', 'super', '0d+ywCe6ffI=', '超级用户', 10, 0, '2', '1', '2', '0', '13802907704', '240823329', null, '240823329@qq.com', null, '1', null, '超级用户，拥有最高的权限', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('06-02-2017 16:56:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 3 records loaded
set feedback on
set define on
prompt Done.
